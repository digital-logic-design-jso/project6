module Multiplier (input [15:0] multiplierInA , MultiplierInB , output[15:0] multiplierOut);
	wire [31:0] a;
	assign a = multiplierInA * MultiplierInB;
	assign multiplierOut = a[23:8];
endmodule