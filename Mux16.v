module Mux16 ( input [15:0] muxInA , muxInX , input seli , selx , output [15:0] muxOut);
	assign muxOut = (selx) ? muxInX : 
						 (seli) ? muxInA : 16'bz ;
endmodule