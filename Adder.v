module Adder (input [15:0] adderInA, adderInB , output [15:0] adderOut);
	assign adderOut = adderInA + adderInB ;
endmodule