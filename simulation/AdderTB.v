module AdderTB();
	reg [15:0] A, B;
	wire [15:0] Out ;
	Adder A1 (A , B , Out);
	initial begin
		A = 16'b0000010100011111;
		B = 16'b1111100101000101;
		#300;
		A = 16'b0011100100000000;
		B = 16'b0000001000001000;
		#300;
		$stop;
	end
endmodule
