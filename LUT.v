module LUT#(parameter n = 16)(Adr , Data);
	input [3:0] Adr;
	output [n-1 : 0] Data;
	
	reg [n-1:0] rom [15:0];
	
	initial begin
		$readmemb("LUT_Data.dat" , rom);
	end
		
	assign Data = rom[Adr][n-1:0];
	

endmodule