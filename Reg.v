module Reg (input [15:0] regInA, input ldA, Ai1, clk, rst, output reg [15:0] regOut);
	always @(posedge clk, posedge rst) begin
		if(rst) regOut <= 16'b0;
		else if(ldA) regOut <= regInA;
		else if(Ai1) regOut <= 16'b0000000100000000;
	end
endmodule