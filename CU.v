module CU (input clk, rst, start, output reg xld, tld, ti1, rld, ri1, seli, selx, busy , ready, output reg [3:0] CNT);
	parameter [3:0] idle = 0, init = 1, getData = 2, calP1 = 3, calP2 = 4, calP3 = 5, calP4 = 6, calP5 = 7;
	reg [3:0] Ps, Ns;
	reg inci , initi;
	wire ico;
	always @(start , Ps , ico) begin
		{busy,xld,tld,rld,selx,seli,ready,inci,ri1,ti1,initi}=11'b0;
		case(Ps)
			idle : begin

				if(start)
					Ns = init;
				else
					Ns = idle;
					
				ready = 1;
				initi = 1;
				busy = 0;
			
			end
			
			init : begin
			
				if(start)
					Ns = init;
				else
					Ns = getData;
					
				ti1 = 1;
				ri1 = 1;
				busy = 1;
			
			end
			
			getData : begin
			
				Ns = calP1;
				xld = 1;
				busy = 1;
			
			end
			
			calP1 : begin
			
				Ns = calP2;
				selx = 1;
				inci = 1;
				busy = 1;
				tld = 1;
			
			end
			
			
			calP2 : begin
			
				Ns = calP3;
				selx = 1;
				busy = 1;
				tld = 1;
			
			end
			
			calP3 : begin
			
				Ns = calP4;
				inci = 1;
				seli = 1;
				busy = 1;
				tld = 1;
			
			end
			
			calP4 : begin
			
				Ns = calP5;
				seli = 1;
				busy = 1;
				tld = 1;
			
			end
			
			calP5 : begin
			
				if(ico)
					Ns = idle;
				else 
					Ns = calP1;
					
				busy = 1;
				rld = 1;
			
			end

			default : Ns = idle;
		
		endcase
	
	end
	
	always @(posedge clk) begin
      if(rst) CNT <= 4'b0000;
		if(initi) CNT <= 4'b0000;
      if(inci & (~ico) ) CNT <= CNT + 1; 
   end
   assign ico = (CNT == 4'b1110) ? 1'b1 : 1'b0;
	
	
	always @(posedge clk , posedge rst)begin
		if(rst) Ps<= 3'b0;
		else Ps<= Ns;
   end
endmodule
